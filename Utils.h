/***********************************************************************
Name:           Thi Thanh Nguyen Nguyen
Student Number: 144128188
Email:          ttnnguyen1@myseneca.ca
Section:        NFF
Date:		    July 30, 2020
========================================
Project:     	1
Part:     	    MS6
***********************************************************************/

/*I have done all the coding by myself and only copied the code that
my professor provided to complete my workshops and assignments.*/

#ifndef SDDS_UTILS_H
#define SDDS_UTILS_H

namespace sdds
{
	int getInt(void);
	int getNumInRange(int min, int max);
	int getSelection(void);
	void pause();
}
#endif // !SDDS_UTILS_H
