/***********************************************************************
Name:           Thi Thanh Nguyen Nguyen
Student Number: 144128188
Email:          ttnnguyen1@myseneca.ca
Section:        NFF
Date:		    July 30, 2020
========================================
Project:     	1
Part:     	    MS6
***********************************************************************/

/*I have done all the coding by myself and only copied the code that
my professor provided to complete my workshops and assignments.*/

#ifndef SDDS_VEHICLE_H
#define SDDS_VEHICLE_H
#include"ReadWritable.h"
#include"Utils.h"
namespace sdds
{
	const int MAX_PLATE = 8;
	class Vehicle: public ReadWritable
	{
		char m_licensePlate[MAX_PLATE +1];
		char* m_make_model;
		int m_parPotNo;
	protected:
		void setEmpty();
		bool isEmpty() const;
		const char* getLicensePlate() const;
		const char* getMakeModel() const;
		void setMakeModel(const char* another);
	public:
		Vehicle();
		Vehicle(const char* licensePlate, const char* make_model);
		Vehicle(const Vehicle& other) = delete;
		Vehicle& operator=(const Vehicle& other) = delete;
		~Vehicle();
		int getParkingSpot() const;
		void setParkingSpot(int value);
		bool operator==(const Vehicle& other) const;
		bool operator==(const char* otherValue) const;
		std::ostream& write(std::ostream& os = std::cout) const;
		std::istream& read(std::istream& in = std::cin);
	};

}
#endif // !SDDS_VEHICLE_H
