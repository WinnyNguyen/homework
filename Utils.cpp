/***********************************************************************
Name:           Thi Thanh Nguyen Nguyen
Student Number: 144128188
Email:          ttnnguyen1@myseneca.ca
Section:        NFF
Date:		    July 30, 2020
========================================
Project:     	1
Part:     	    MS6
***********************************************************************/

/*I have done all the coding by myself and only copied the code that
my professor provided to complete my workshops and assignments.*/

#define _CRT_SECURE_NO_WARNINGS
#include<iostream>
#include<cstring>
#include "Utils.h"
using namespace std;
namespace sdds
{
	int getInt(void)
	{
		int value;
		while (!(cin >> value))
		{
			cout << "Invalid Integer, try again: ";
			cin.clear();
			cin.ignore(2000, '\n');
		}
		if (cin) cin.ignore(2000, '\n');
		return value;
	}
	int getNumInRange(int min, int max)
	{
		int done = 0, value;
		do
		{
			value = getInt();
			if (value < min || value > max)
			{
				done = 1;
				cout << "Invalid selection, try again: ";
			}
			else done = 0;
		} while (done);

		return value;
	}

	int getSelection(void) {
		int result = 0;
		int flag = 0;
		char check[4] = { '\0' };
		do {
			cin.get(check, 3);
			if ((check[0] != 'y' && check[0] != 'Y' && check[0] != 'N' && check[0] != 'n') || strlen(check) > 1) {
				check[0] = '\0';
				cin.clear();
				cin.ignore(2000, '\n');
				cout << "Invalid response, only (Y)es or (N)o are acceptable, retry: ";
				flag = 1;
			}
			else if (check[0] == 'Y' || check[0] == 'y') {
				result = 1;
				flag = 0;
			}
			else if (check[0] == 'N' || check[0] == 'n') {
				result = 0;
				flag = 0;
			}
		} while (flag);
		check[0] = '\0';
		return result;
	}

	void pause() {
		cout << "<------------------------" << endl;
		cout << "Press enter to continue...";
		cin.ignore();
		cin.ignore(1000, '\n');
	}
}