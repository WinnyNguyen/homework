/***********************************************************************
Name:           Thi Thanh Nguyen Nguyen
Student Number: 144128188
Email:          ttnnguyen1@myseneca.ca
Section:        NFF
Date:		    July 30, 2020
========================================
Project:     	1
Part:     	    MS6
***********************************************************************/

#ifndef SDDS_PARKING_H
#define SDDS_PARKING_H
#include "Menu.h"
#include"ReadWritable.h"
#include"Utils.h"
#include"Vehicle.h"
#include"Car.h"
#include"Motorcycle.h"

namespace sdds
{
	const int MAX_PARK_SPOT = 100;
	class Parking
	{
		char* m_fileName;
		int m_noOfSpots;
		int m_noOfParkedVehicles;
		Menu m_parkingMenu;
		Menu m_vehicleSelection;
		Vehicle* m_parkingSpot[MAX_PARK_SPOT];
	
		Parking(const Parking& other) = delete;
		Parking& operator=(const Parking& other) = delete;
		bool isEmpty() const;
		void parkingStatus() const;
		void parkVehicle();
		void returnVehicle();
		void listParkedVehicle() const;
		bool closeParking();
		bool exitParkingApp();
		bool loadDataFile();
		void saveDataFile();
	
	public:
		Parking();
		Parking(const char* datafile, int noOfSpots);
		~Parking();
		void clearMember();
		int run();
	};
	//Vehicle* create();
}

#endif // !SDDS_PARKING_H