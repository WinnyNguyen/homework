/***********************************************************************
Name:           Thi Thanh Nguyen Nguyen
Student Number: 144128188
Email:          ttnnguyen1@myseneca.ca
Section:        NFF
Date:		    July 30, 2020
========================================
Project:     	1
Part:     	    MS6
***********************************************************************/

/*I have done all the coding by myself and only copied the code that
my professor provided to complete my workshops and assignments.*/

#ifndef SDDS_MOTORCYCLE_H_
#define SDDS_MOTORCYCLE_H_
#include"Vehicle.h"

namespace sdds
{
	class Motorcycle : public Vehicle
	{
		bool sidecarFlag;
	public:
		Motorcycle();
		Motorcycle(const char* licensePlate, const char* make_model);
		Motorcycle(const Motorcycle& other) = delete;
		Motorcycle& operator=(const Motorcycle& other) = delete;
		std::ostream& write(std::ostream& os = std::cout) const;
		std::istream& read(std::istream& in = std::cin);
	};
}
#endif // !SDDS_MOTORCYCLE_H_