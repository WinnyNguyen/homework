﻿/***********************************************************************
Name:           Thi Thanh Nguyen Nguyen
Student Number: 144128188
Email:          ttnnguyen1@myseneca.ca
Section:        NFF
Date:		    July 30, 2020
========================================
Project:     	1
Milestone:     	MS6
***********************************************************************/

#include <iostream>
#ifndef SDDS_READWRITABLE_H
#define SDDS_READWRITABLE_H

namespace sdds
{
	class ReadWritable 
	{
		bool m_values_flag;
	public:
		ReadWritable();
		bool isCsv()const;
		void setCsv(bool value);
		virtual ~ReadWritable();
		virtual std::ostream& write(std::ostream& = std::cout) const = 0;
		virtual std::istream& read(std::istream& in = std::cin) = 0;
		bool isValid() const;
	};
	std::istream& operator>>(std::istream& is, ReadWritable& s);
	std::ostream& operator<<(std::ostream& os, const ReadWritable& s);
}
#endif // !SDDS_READWRITABLE_H