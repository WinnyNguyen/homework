/***********************************************************************
Name:           Thi Thanh Nguyen Nguyen
Student Number: 144128188
Email:          ttnnguyen1@myseneca.ca
Section:        NFF
Date:		    July 30, 2020
========================================
Project:     	1
Milestone:     	MS6
***********************************************************************/

#include <iostream>
#include "ReadWritable.h"

using namespace std;

namespace sdds
{
	ReadWritable::ReadWritable() {
		this->m_values_flag = false;
	}

	bool ReadWritable::isCsv()const {
		return this->m_values_flag;
	}

	void ReadWritable::setCsv(bool value) {
		this->m_values_flag = value;
	}

	bool ReadWritable::isValid() const {
		return this->m_values_flag;
	}
	ReadWritable::~ReadWritable() {
	}

	istream& operator>>(istream& is, ReadWritable& s) {
		s.read(is);
		return is;
	}

	ostream& operator<<(ostream& os, const ReadWritable& s) {
		s.write(os);
		return os;
	}
}