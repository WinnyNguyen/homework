/***********************************************************************
Name:           Thi Thanh Nguyen Nguyen
Student Number: 144128188
Email:          ttnnguyen1@myseneca.ca
Section:        NFF
Date:		    July 30, 2020
========================================
Project:     	1
Part:     	    MS6
***********************************************************************/

/*I have done all the coding by myself and only copied the code that
my professor provided to complete my workshops and assignments.*/

#include<iostream>
#include "Car.h"

using namespace std;

namespace sdds
{
	Car::Car() {
		this->carWashFlag = false;
	}

	Car::Car(const char* licensePlate, const char* make_model) :Vehicle(licensePlate, make_model) {};

	ostream& Car::write(ostream& os) const {
		if (this->isEmpty()) os << "Invalid Car Object" << endl;
		else {
			if (this->isValid()) os << "C,";
			else os << "Vehicle type: Car" << endl;
			Vehicle::write(os);
			if (this->isValid()) os << this->carWashFlag << endl;
			else {
				if (this->carWashFlag) os << "With Carwash" << endl;
				else os << "Without Carwash" << endl;
			}
		}
		return os;
	}

	istream& Car::read(istream& in) {
		if (!this->isCsv()) cout << "\nCar information entry" << endl;
		Vehicle::read(in);
		if (this->isValid()) {
			in >> this->carWashFlag;
			in.ignore(2000, '\n');
		}
		else {
			cout << "Carwash while parked? (Y)es/(N)o: ";
			int getOption = getSelection();
			if (getOption) this->carWashFlag = true;
			else this->carWashFlag = false;
		}
		return in;
	}
}