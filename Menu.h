/***********************************************************************
Name:           Thi Thanh Nguyen Nguyen
Student Number: 144128188
Email:          ttnnguyen1@myseneca.ca
Section:        NFF
Date:		    July 30, 2020
========================================
Project:     	1
Part:     	    MS6
***********************************************************************/

/*I have done all the coding by myself and only copied the code that
my professor provided to complete my workshops and assignments.*/

#ifndef SDDS_MENU_H
#define SDDS_MENU_H
#include<iostream>
#include "Utils.h"
namespace sdds
{
	const int MAX_NO_OF_ITEMS = 10;
	class Menu;
	class  MenuItem {
		friend class Menu;
		MenuItem();
		char* m_name;
		MenuItem(const MenuItem& M) = delete;
		MenuItem& operator=(const MenuItem& M) = delete;
		explicit MenuItem(const char* name);
		void displayItem(std::ostream& os = std::cout) const;
		bool isValid() const;
		~MenuItem();
	};
	class Menu {
		char* m_title;
		MenuItem* m_list[MAX_NO_OF_ITEMS];
		int m_indent;
		int size_list;
	public:
		Menu();
		Menu(const char* title, int indent = 0);
		Menu(const Menu&);
		explicit operator bool() const;
		operator int() const;
		bool isEmpty() const;
		void display(std::ostream& os2 = std::cout) const;
		Menu& operator=(const char* n_title);
		Menu& operator=(const Menu& other);
		Menu& operator<<(const char* n_title);
		void add(const char* title);
		void printIndent() const;
		int run() const;
		void clear();
		~Menu();
	};
}
#endif // !SDDS_MENU_H
