/***********************************************************************
Name:           Thi Thanh Nguyen Nguyen
Student Number: 144128188
Email:          ttnnguyen1@myseneca.ca
Section:        NFF
Date:		    July 30, 2020
========================================
Project:     	1
Part:     	    MS6
***********************************************************************/

/*I have done all the coding by myself and only copied the code that
my professor provided to complete my workshops and assignments.*/

#define _CRT_SECURE_NO_WARNINGS
#include<iostream>
#include<cstring>
#include "Vehicle.h"

using namespace std;

namespace sdds
{
	Vehicle::Vehicle() {
		this->setEmpty();
	}

	Vehicle::Vehicle(const char* licensePlate, const char* make_model) {
		if (licensePlate == nullptr || make_model == nullptr || licensePlate[0] == '\0' || make_model[0] == '\0' || strlen(make_model) < 2 || (make_model[0] >= 48 && make_model[0] <= 57) || (licensePlate[0] >= 48 && licensePlate[0] <= 57))
			this->setEmpty();

		else {
			size_t length = strlen(make_model);
			this->m_make_model = new char[length + 1];
			strcpy(this->m_make_model, make_model);

			strncpy(this->m_licensePlate, licensePlate, 8);

			this->m_parPotNo = 0;
		}
	}

	Vehicle::~Vehicle() {

		delete[] this->m_make_model;
		this->m_make_model = nullptr;
	}

	void Vehicle::setEmpty() {
		this->m_licensePlate[0] = '\0';
		this->m_make_model = nullptr;
		this->m_parPotNo = 0;
	}

	bool Vehicle::isEmpty() const {
		return (this->m_licensePlate == nullptr || this->m_make_model == nullptr || this->m_licensePlate[0] == '\0' || this->m_make_model[0] == '\0');
	}

	const char* Vehicle::getLicensePlate() const {
		return this->m_licensePlate;
	}

	const char* Vehicle::getMakeModel() const {
		return this->m_make_model;
	}

	void Vehicle::setMakeModel(const char* another) {
		if (another == nullptr || another[0] == '\0' || strlen(another) < 2 || strlen(another) > 60 || (another[0] >= 48 && another[0] <= 57)) {
			this->setEmpty();
		}
		else {
			if (this->m_make_model != nullptr) {
				delete[] this->m_make_model;
				this->m_make_model = nullptr;
			}
			size_t length = strlen(another);
			this->m_make_model = new char[length + 1];
			strcpy(this->m_make_model, another);
		}
	}

	int Vehicle::getParkingSpot() const {
		return this->m_parPotNo;
	}

	void Vehicle::setParkingSpot(int value) {
		if (value < 0) {
			if (this->m_make_model != nullptr) {
				delete[] this->m_make_model;
				this->m_make_model = nullptr;
			}
			this->m_parPotNo = 0;
			this->m_licensePlate[0] = '\0';
		}
		else this->m_parPotNo = value;
	}

	bool Vehicle::operator==(const Vehicle& other) const{
		bool result = false;
		if (!other.isEmpty()) {
			result = *this == other.m_licensePlate ? true : false;
		}
		return result;
	}

	bool Vehicle::operator==(const char* otherValue) const {
		bool result = false;
		if (otherValue != nullptr && otherValue[0] != '\0') {
			char temp1[8] = { '\0' };
			char temp2[8] = { '\0' };
			for (unsigned i = 0; i < strlen(this->m_licensePlate); i++) {
				temp1[i] = tolower(this->m_licensePlate[i]);
			}
			for (unsigned i = 0; i < strlen(otherValue); i++) {
				temp2[i] = tolower(otherValue[i]);
			}
			int check = strcmp(temp1, temp2);
			if (check == 0) result = true;
		}
		return result;
	}

	ostream& Vehicle::write(ostream& os) const {
		if (this->isEmpty()) os << "Invalid Vehicle Object" << endl;
		else {
			if (this->isValid()) {
				os << this->m_parPotNo << "," << this->m_licensePlate << "," << this->m_make_model << ",";
			}
			else {
				if (this->m_parPotNo == 0)
					os << "Parking Spot Number: " << "N/A" << endl;
				else os << "Parking Spot Number: " << this->m_parPotNo << endl;
				os << "Licence Plate: " << this->m_licensePlate << endl;
				os << "Make and Model: " << this->m_make_model << endl;
			}
		}
		return os;
	}
	istream& Vehicle::read(istream& in) {
		char temp1[20] = { '\0' };
		char temp2[70] = { '\0' };
		int pot;
		if (this->isCsv()) {
			in >> pot;
			in.ignore();
			in.getline(temp1, 9, ',');
			for (unsigned i = 0; i < strlen(temp1); i++) {
				temp1[i] = toupper(temp1[i]);
			}
			in.getline(temp2, 61, ',');
			Vehicle temp(temp1, temp2);
			if (!temp.isEmpty()) {
				if (this->m_licensePlate != nullptr) {
					this->m_licensePlate[0] = '\0';
				}
				strcpy(this->m_licensePlate, temp1);

				this->setMakeModel(temp2);
				this->setParkingSpot(pot);

				temp1[0] = '\0';
				temp2[0] = '\0';
			}
		}
		else {
			int flag = 0;
			cout << "Enter Licence Plate Number: ";
			do {
				in.getline(temp1, '\n');
				if (strlen(temp1) > 8 || strlen(temp1) < 1) {
					cout << "Invalid Licence Plate, try again: ";
					flag = 1;
					temp1[0] = '\0';
				}
				else {
					flag = 0;
					for (unsigned i = 0; i < strlen(temp1); i++) {
						if (islower(temp1[i]))
							temp1[i] = char(toupper(temp1[i]));
					}
				}
			} while (flag);

			cout << "Enter Make and Model: ";
			do {
				in.getline(temp2, 60, '\n');
				if (strlen(temp2) > 60 || strlen(temp2) < 2) {
					cout << "Invalid Make and model, try again: ";
					flag = 1;
					temp2[0] = '\0';
				}
				else flag = 0;
			} while (flag);

			Vehicle temp(temp1, temp2);
			if (!temp.isEmpty()) {
				this->setMakeModel(temp2);
				this->setParkingSpot(0);

				if (this->m_licensePlate != nullptr) {
					this->m_licensePlate[0] = '\0';
				}
				strcpy(this->m_licensePlate, temp1);
				temp1[0] = '\0';
				temp2[0] = '\0';
			}
		}
		return in;
	}
}