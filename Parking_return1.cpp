/***********************************************************************
Name:           Thi Thanh Nguyen Nguyen
Student Number: 144128188
Email:          ttnnguyen1@myseneca.ca
Section:        NFF
Date:		    July 30, 2020
========================================
Project:     	1
Part:     	    MS6
***********************************************************************/
#define _CRT_SECURE_NO_WARNINGS
#include <fstream>
#include <iostream>
#include <cstring>
#include <iomanip>
#include "Parking.h"
#include "Car.h"
#include "Motorcycle.h"

using namespace std;

namespace sdds
{
	Menu parkingMenu("Parking Menu, select an action:", 0);
	Menu vehicleMenu("Select type of the vehicle:", 1);

	Parking::Parking() {
		this->m_fileName = nullptr;
		this->m_parkingMenu = nullptr;
		this->m_vehicleSelection = nullptr;
		this->m_noOfParkedVehicles = 0;
		this->m_noOfSpots = 0;
		for (int i = 0; i < MAX_PARK_SPOT; i++) {
			this->m_parkingSpot[i] = nullptr;
		}
	}

	Parking::Parking(const char* datafile, int noOfSpots) {
		if (noOfSpots < 10 || noOfSpots > MAX_PARK_SPOT || datafile[0] == '\0' || datafile == nullptr) {
			this->clearMember();
			cout << "Error in data file" << endl;
		}
		else {
			this->m_noOfSpots = noOfSpots;
			this->m_noOfParkedVehicles = 0;
			for (int i = 0; i < MAX_PARK_SPOT; i++) {
				this->m_parkingSpot[i] = nullptr;
			}
			this->m_fileName = new char[strlen(datafile) + 1];
			strcpy(this->m_fileName, datafile);
			if (this->loadDataFile()) {
				this->m_parkingMenu = parkingMenu;
				this->m_parkingMenu << "Park Vehicle" << "Return Vehicle" << "List Parked Vehicles" << "Close Parking (End of day)" << "Exit Program";
				this->m_vehicleSelection = vehicleMenu;
				this->m_vehicleSelection << "Car" << "Motorcycle" << "Cancel";
			}
			else {
				this->m_parkingMenu = nullptr;
				this->m_vehicleSelection = nullptr;
			}
		}
	}
	bool Parking::isEmpty() const {
		bool result = false;
		if (this->m_fileName == nullptr || this->m_fileName[0] == '\0') result = true;
		return result;
	}

	void Parking::parkingStatus() const {
		cout << "****** Seneca Valet Parking ******" << endl;
		cout << "*****  Available spots: ";
		cout << left << setw(4) << this->m_noOfSpots + 1 - this->m_noOfParkedVehicles;
		cout << " *****" << endl;
	}

	void Parking::parkVehicle() {
		if (this->m_noOfParkedVehicles >= this->m_noOfSpots + 1) {
			cout << "Parking is full" << endl;
		}
		else {
			int option = this->m_vehicleSelection.run();
			switch (option) {
			case 1:
				this->m_noOfParkedVehicles += 1;
				for (int i = 0; i < MAX_NO_OF_ITEMS; i++) {
					if (this->m_parkingSpot[i] == nullptr) {
						this->m_parkingSpot[i] = new Car;
						this->m_parkingSpot[i]->setCsv(false);
						this->m_parkingSpot[i]->read(std::cin);
						this->m_parkingSpot[i]->setParkingSpot(i + 1);
						cout << endl << "Parking Ticket" << endl;
						this->m_parkingSpot[i]->write(std::cout);
						cout << endl;
						break;
					}
				}
				break;
			case 2:
				this->m_noOfParkedVehicles += 1;
				for (int i = 0; i < MAX_NO_OF_ITEMS; i++) {
					if (this->m_parkingSpot[i] == nullptr) {
						this->m_parkingSpot[i] = new Motorcycle;
						this->m_parkingSpot[i]->setCsv(false);
						this->m_parkingSpot[i]->read(std::cin);
						this->m_parkingSpot[i]->setParkingSpot(i + 1);
						cout << endl << "Parking Ticket" << endl;
						this->m_parkingSpot[i]->write(std::cout);
						cout << endl;
						break;
					}
				}
				break;
			case 3:
				cout << "Parking Cancelled" << endl;
				break;
			default:
				break;
			}
		}
	}

	void Parking::returnVehicle() {
		cout << "Return Vehicle" << endl;
		cout << "Enter Licence Plate Number: ";
		int done = 0;
		char license[15] = { '\0' };
		do {
			cin.getline(license, 15, '\n');
			if (strlen(license) > 9) {
				cout << "Invalid Licence Plate, try again: ";
				license[0] = { '\0' };
				done = 1;
			}
		} while (done);
		int check = 0;
		for (int t = 0; t < this->m_noOfSpots + 1; t++) {
			if (*this->m_parkingSpot[t] == license) {
				check += 1;
				this->m_noOfParkedVehicles--;
				cout << endl << "Returning: " << endl;
				this->m_parkingSpot[t]->setCsv(false);
				this->m_parkingSpot[t]->write(std::cout);

				delete this->m_parkingSpot[t];
				this->m_parkingSpot[t] = nullptr;
<<<<<<< HEAD
				break;
			}
		
=======
				this->m_parkingSpot[t]->setParkingSpot(0);
			
			}
			
>>>>>>> af545f410325f15ae9b17b9dca96f8bcff4e18ff
		}
		if (check == 0)
			cout << "License plate " << license << " Not found" << endl;
		license[0] = { '\0' };

	}

	void Parking::listParkedVehicle() const {
		cout << "*** List of parked vehicles ***" << endl;
		for (int i = 0; i < MAX_PARK_SPOT; i++) {

			if (this->m_parkingSpot[i] != nullptr)
			{
				this->m_parkingSpot[i]->setCsv(false);
				this->m_parkingSpot[i]->write(std::cout);
				cout << "-------------------------------" << endl;
			}
		}
	}

	bool Parking::closeParking() {
		bool result = true;
		if (this->m_noOfParkedVehicles == 0) {
			cout << "Closing Parking" << endl;
		}
		else {
			cout << "This will Remove and tow all remaining Vehicles from the Parking!" << endl;
			cout << "Are you sure? (Y)es/(N)o: ";
			int answer = getSelection();
			if (!answer) {
				result = false;
				cout << "Aborted!" << endl;
			}
			else {
				cout << "Closing Parking" << endl;

				for (int i = 0; i < MAX_NO_OF_ITEMS; i++) {
					cout << "Towing request" << endl;
					cout << "*********************" << endl;
					if (this->m_parkingSpot[i] != nullptr) {
						this->m_parkingSpot[i]->write(std::cout);
						cout << endl;
						delete this->m_parkingSpot[i];
						this->m_parkingSpot[i] = nullptr;
						this->m_noOfParkedVehicles -= 1;
					}
				}
			}
		}

		return result;
	}

	bool Parking::exitParkingApp() {
		bool result = false;
		cout << "This will terminate the program!" << endl;
		cout << "Are you sure? (Y)es/(N)o: ";
		int selection = getSelection();
		if (selection) result = true;
		return result;
	}

	bool Parking::loadDataFile() {
		bool result = false;
		ifstream fin(this->m_fileName);
		char code;
		if (this->isEmpty() || !fin.is_open()) result = false;
		else {
			while (fin) {
				fin >> code;
				fin.ignore();
				if (!fin) break;
				if (code == 'M' && this->m_noOfParkedVehicles < this->m_noOfSpots + 1) {
					this->m_noOfParkedVehicles += 1;
					for (int i = 0; i < MAX_PARK_SPOT; i++) {
						if (this->m_parkingSpot[i] == nullptr) {
							this->m_parkingSpot[i] = new Motorcycle;
							this->m_parkingSpot[i]->setCsv(true);
							this->m_parkingSpot[i]->read(fin);
							break;
						}
					}
				}
				else if (code == 'C' && this->m_noOfParkedVehicles < this->m_noOfSpots + 1) {
					this->m_noOfParkedVehicles += 1;
					for (int i = 0; i < MAX_PARK_SPOT; i++) {
						if (this->m_parkingSpot[i] == nullptr) {
							this->m_parkingSpot[i] = new Car;
							this->m_parkingSpot[i]->setCsv(true);
							this->m_parkingSpot[i]->read(fin);
							break;
						}
					}
				}
			}
			result = true;
		}
		fin.close();
		return result;
	}

	void Parking::saveDataFile() {
		ofstream fout("ParkingData.csv");
		if (!this->isEmpty() && fout.is_open()) {
			for (int i = 0; i < this->m_noOfSpots + 1; i++) {
				if (this->m_parkingSpot[i] != nullptr) {
					this->m_parkingSpot[i]->setCsv(true);
					this->m_parkingSpot[i]->write(fout);
				}
			}
			cout << "Saving data into " << m_fileName << endl;
			fout.close();
		}
		else cout << "Can not open file or this object is empty" << endl;
	}

	int Parking::run() {
		int result = 0;
		//static int pass = 0;
		int flag = 1;
		int checkExit;
		int checkClose;

		//cout << "Pass " << pass + 1 << " ------->" << endl;
		if (!this->isEmpty()) {
			do {
				parkingStatus();
				int selection = this->m_parkingMenu.run();
				switch (selection) {
				case 1:
					parkVehicle();
					break;
				case 2:
					returnVehicle();
					break;
				case 3:
					listParkedVehicle();
					break;
				case 4:
					checkClose = closeParking();
					if (checkClose) {
						flag = 0;
						//pass += 1;
					}
					break;
				case 5:
					checkExit = exitParkingApp();
					if (checkExit) {
						flag = 0;
						cout << "Exiting program!" << endl;
						//pass += 1;
					}
					break;
				default:
					flag = 1;
					break;
				}
			} while (flag);
		}
		if (this->isEmpty()) {
			result = 1;
		}

		return result;
	}

	void Parking::clearMember() {
		if (this->m_fileName != nullptr)
			delete[] this->m_fileName;
		this->m_fileName = nullptr;
		this->m_parkingMenu = nullptr;
		this->m_vehicleSelection = nullptr;
		this->m_noOfParkedVehicles = 0;
		this->m_noOfSpots = 0;
		for (int i = 0; i < MAX_PARK_SPOT; i++) {
			if (this->m_parkingSpot[i] != nullptr)
				delete this->m_parkingSpot[i];
			this->m_parkingSpot[i] = nullptr;
		}
	}

	Parking::~Parking() {
		saveDataFile();
		delete[] m_fileName;
		m_fileName = nullptr;
		for (int i = 0; i < MAX_PARK_SPOT; i++) {
			if (this->m_parkingSpot[i] != nullptr)
				delete this->m_parkingSpot[i];
			this->m_parkingSpot[i] = nullptr;
		}
	}
}