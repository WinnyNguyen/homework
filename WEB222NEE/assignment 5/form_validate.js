var select1 = document.getElementById('question');
select1.addEventListener('click', hideBox);

var select2 = document.getElementById('comment');
select2.addEventListener('click', hideBox);

var select3 = document.getElementById('problem');
select3.addEventListener('click', displayBox);

function displayBox() {
  var box_ = document.querySelector('textarea[name=box]');
  box_.required = true;
  box_.disabled = false;
  box_.classList.add('show_box');
}

function hideBox() {
  var box_ = document.querySelector('textarea[name=box]');
  box_.innerHTML = '';
  box_.disabled = true;
  box_.classList.remove('show_box');
}
var validate_all = document.main_form;
validate_all.addEventListener('submit', validate());

function validate() {
  // clearErrors(); // Clear error message area
  // var result = validate_name(); // Call name validation function
  //  result = validate_phone(); // Call phone number validation function
  // result = validate_add() && result;
  // result = validate_com() && result;
  return validate_add() && validate_com() && validate_name();
  // return result;
}

function validate_name() {
  var nameMessageRules = '<p> - Please enter a minimum of 4 alphabetic characters</p>';

  var stringName = document.querySelector('#fullName').value;
  stringName = stringName.trim();
  var stringLength = stringName.length;

  if (stringLength == 0) {
    showErrors(
      '<p><mark>Name</mark><br /> - The name field can not be left empty or just blank characters<br />' +
        nameMessageRules +
        '</p>'
    );
    return false;
  }

  if (stringLength < 4) {
    showErrors(
      '<p><mark>Name</mark><br /> - You did not enter enough characters for the name<br />' +
        nameMessageRules +
        '</p>'
    );
    return false;
  }

  var countNonAlpha = 0;
  stringName = stringName.toUpperCase();
  for (var i = 0; i < stringLength; i++) {
    if (!(stringName.charCodeAt(i) > 64 && stringName.charCodeAt(i) < 91)) {
      countNonAlpha++;
      break;
    }
  }

  if (countNonAlpha) {
    showErrors(
      '<p><mark>Name</mark><br /> - Only alphabetic characters are allowed for the name<br />' +
        nameMessageRules +
        '</p>'
    );
    return false;
  }
  return true;
}

// function validate_phone() {
//   var messageRules = '<p> - Please enter a phone number with the format of ###-###-####.</p>';

//   var str = document.querySelector('#phone').value;
//   str = str.trim();

//   var stringLength = str.length;

//   if (stringLength === 0) {
//     showErrors(
//       '<p><mark>Phone Number</mark><br /> - The phone number field can not be left empty or just blank characters<br />' +
//         messageRules +
//         '</p>'
//     );
//     return false;
//   }

//   if (str.charAt(3) !== '-' || str.charAt(7) !== '-' || stringLength !== 12) {
//     showErrors(
//       '<p><mark>Phone Number</mark><br /> - The phone number was in wrong format.<br />' +
//         messageRules +
//         '</p>'
//     );
//     return false;
//   } else if (str.charAt(3) === '-' && str.charAt(7) === '-' && stringLength === 12) {
//     var i,
//       flag = true,
//       myArray = str.split('-');
//     for (i = 0; i < 3; i++) {
//       if (parseInt(myArray[i]) !== myArray[i]) {
//         flag = false;
//         break;
//       }
//     }

//     if (!flag) {
//       showErrors(
//         '<p><mark>Phone Number</mark><br /> - The phone number was in wrong format.<br />' +
//           messageRules +
//           '</p>'
//       );
//       return false;
//     }
//   }
//   return true;
// }

function validate_add() {
  var check_alpha = false;
  var check_num = false;
  var string1 = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
  var string2 = '0123456789';
  var get_value = document.main_form.address.value.trim();
  var alert_1 = document.getElementById('alert1');

  for (var i = 0; i < get_value.length; i++) {
    if (string1.indexOf(get_value.substr(i, 1)) >= 0) check_alpha = true;
    if (string2.indexOf(get_value.substr(i, 1)) >= 0) check_num = true;
  }
  if (check_alpha && check_num) {
    return true;
  }
  alert_1.innerHTML = 'The address does not look good, please check again';
  document.main_form.address.focus();
  return false;
}

function validate_com() {
  var input = document.main_form.option.value.trim();
  var alert_2 = document.getElementById('alert2');
  if (input.length > 0) {
    return true;
  }
  alert_2.innerHTML = 'Please enter your comment!';
  input.focus();
  return false;
}

function clearErrors() {
  document.getElementById('alert1').innerHTML = '';
  document.getElementById('alert2').innerHTML = '';
  document.getElementById('errors').innerHTML = '';
  hideBox();
  document.querySelector('#FullName').focus();
}

function showErrors(messages) {
  document.querySelector('#errors').innerHTML += messages;
}
