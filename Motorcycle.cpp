/***********************************************************************
Name:           Thi Thanh Nguyen Nguyen
Student Number: 144128188
Email:          ttnnguyen1@myseneca.ca
Section:        NFF
Date:		    July 30, 2020
========================================
Project:     	1
Part:     	    MS6
***********************************************************************/

/*I have done all the coding by myself and only copied the code that
my professor provided to complete my workshops and assignments.*/


#include<iostream>
#include "Motorcycle.h"

using namespace std;

namespace sdds
{
	Motorcycle::Motorcycle() {
		this->sidecarFlag = false;
	}

	Motorcycle::Motorcycle(const char* licensePlate, const char* make_model) : Vehicle(licensePlate, make_model) {}

	ostream& Motorcycle::write(ostream& os) const {
		if (this->isEmpty()) os << "Invalid Motorcycle Object" << endl;

		else {
			if (this->isValid()) os << "M,";
			else os << "Vehicle type: Motorcycle" << endl;
			Vehicle::write(os);
			if (this->isValid()) os << this->sidecarFlag << endl;
			else {
				if (this->sidecarFlag) os << "With Sidecar" << endl;
			}
		}
		return os;
	}

	istream& Motorcycle::read(istream& in) {
		if (!this->isCsv()) cout << "\nMotorcycle information entry" << endl;
		Vehicle::read(in);
		if (this->isValid())
		{
			in >> this->sidecarFlag;
			in.ignore(2000, '\n');
		}
		else {
			cout << "Does the Motorcycle have a side car? (Y)es/(N)o: ";
			int getOption = getSelection();
			if (getOption) this->sidecarFlag = true;
			else this->sidecarFlag = false;
		}
		return in;
	}
}