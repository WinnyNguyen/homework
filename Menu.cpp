/***********************************************************************
Name:           Thi Thanh Nguyen Nguyen
Student Number: 144128188
Email:          ttnnguyen1@myseneca.ca
Section:        NFF
Date:		    July 30, 2020
========================================
Project:     	1
Part:     	    MS6
***********************************************************************/

#define _CRT_SECURE_NO_WARNINGS
#include <iostream>
#include <cstring>
#include "Menu.h"

using namespace std;
namespace sdds{
	MenuItem::MenuItem() {
		this->m_name = nullptr;
	}

	MenuItem::MenuItem(const char* name) {
		if (name == nullptr || name[0] == '\0') {
			this->m_name = nullptr;
		}
		else {
			this->m_name = new char[strlen(name) + 1];
			strcpy(this->m_name, name);
		}
	}

	bool MenuItem::isValid() const {
		return (this->m_name != nullptr && this->m_name[0] != '\0');
	}

	void MenuItem::displayItem(ostream& os) const {
		if (this->isValid()) {
			os << this->m_name << endl;
		}
		else os << "no item available" << endl;
	}

	MenuItem::~MenuItem() {
		delete[] m_name;
		m_name = nullptr;
	}

	Menu::Menu() {
		this->m_title = nullptr;
		this->m_indent = 0;
		this->size_list = 0;
		for (int i = 0; i < MAX_NO_OF_ITEMS; i++) {
			this->m_list[i] = nullptr;
		}
	}

	Menu::Menu(const char* title, int indent) {
		this->m_title = nullptr;
		if (title == nullptr || title[0] == '\0')	*this = Menu();
		else {
			this->m_indent = indent;
			*this = title; // call function of operator=(const char* n_title);
			this->size_list = 0;
			for (int i = 0; i < MAX_NO_OF_ITEMS; i++) this->m_list[i] = nullptr;
		}
	}

	Menu::Menu(const Menu& newMenu) {
		this->m_title = nullptr;
		this->size_list = 0;
		this->clear();// call clear function to clear all the elements of m_list.
		*this = newMenu;// call function of operator=(const Menu& other)
	}

	Menu& Menu::operator=(const Menu& other) {
		if (this != &other) {
			this->m_indent = other.m_indent;
			if (this->m_title != nullptr) {
				delete[] this->m_title;
				this->m_title = nullptr;
			}
			if (other.m_title != nullptr) {
				this->m_title = new char[strlen(other.m_title) + 1];
				strcpy(this->m_title, other.m_title);
			}
			else this->m_title = nullptr;
			this->clear();// call clear function to clear all the elements of m_list.
			size_list = 0;
			for (int i = 0; i < other.size_list; i++) {
				if (other.m_list[i] != nullptr) 
					add(other.m_list[i]->m_name);//call add function to add items.
			}
		}
		return *this;
	}

	Menu::operator bool() const {
		return (this->m_title != nullptr && this->m_title[0] != '\0');
	}

	bool Menu::isEmpty() const {
		return(this->m_title == nullptr || this->m_title[0] == '\0');
	}

	void Menu::display(std::ostream& os) const {
		this->printIndent();
		if (this->isEmpty()) os << "Invalid Menu!" << endl;
		else if (this->size_list == 0) {
			os << this->m_title << endl;
			os << "No Items to display!" << endl;
		}
		else {
			os << this->m_title << endl;
			for (int i = 0; i < size_list; i++) {
				this->printIndent();
				os << i + 1 << "- ";
				this->m_list[i]->displayItem();
			}
			this->printIndent();
			os << "> ";
		}
	}

	Menu& Menu::operator=(const char* n_title) {
		if (n_title == nullptr || n_title[0] == '\0')
			*this = Menu(); // calls default contructor
		else {
			if (this->m_title != nullptr) {
				delete[] this->m_title;
				this->m_title = nullptr;
			}
			this->m_title = new char[strlen(n_title) + 1];
			strcpy(this->m_title, n_title);
		}
		return *this;
	}

	Menu& Menu::operator<<(const char* n_title) {
		this->add(n_title);
		return *this;
	}

	void Menu::add(const char* item) {
		if (item == nullptr || item[0] == '\0') 
			*this = Menu();
		else if (this->size_list < MAX_NO_OF_ITEMS) {
			this->m_list[size_list] = new MenuItem(item);//create new MenuItem and add it to m_list.
			this->size_list++;
		}
	}

	int Menu::run() const {
		this->display();
		int option;
		if (this->size_list == 0 || this->isEmpty()) option = 0;
		else option = getNumInRange(1, this->size_list);
		return option;
	}

	void Menu::printIndent() const {
		for (int i = 0; i < m_indent * 4; i++) cout << " ";
	}

	Menu::operator int() const {
		return this->run();
	}

	//clear all the elements of m_list which contains items.
	void Menu::clear() {
		for (int i = 0; i < MAX_NO_OF_ITEMS; i++) {
			delete this->m_list[i];
			this->m_list[i] = nullptr;
		}
	}

	Menu::~Menu() {
		delete[] this->m_title;
		this->m_title = nullptr;
		this->clear(); // call clear function to clear all the elements of m_list.
	}
}