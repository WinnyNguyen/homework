/***********************************************************************
Course		: DBS 211 NBB
Group		: 7
Date		: August 07, 2020
=========================================================================
Student 1	: Hoang Lan Anh Vu - hlavu@myseneca.ca - 137393195
Student 2	: Thi Thuy Trang Nguyen - tnguyen-thi-thuy@myseneca.ca - 131104192
Student 3	: Thi Thanh Nguyen Nguyen - ttnnguyen1@myseneca.ca - 144128188
=========================================================================
Assignment	: 1
Milestone	: 2
* **********************************************************************/

#define _CRT_SECURE_NO_WARNINGS
#include <iostream>
#include <iomanip>
#include <cstring>
#include <occi.h>

using oracle::occi::Environment;
using oracle::occi::Connection;

using namespace oracle::occi;
using namespace std;
int menu(void);
int getSelection(void);
int getNumInRange(int min, int max);
int getInt(void);
void displayStr(int width, string content);
void displayNum(int width, const int content);

struct Employee
{
	int employeeNumber;
	char lastName[50];
	char firstName[50];
	char email[100];
	char phone[50];
	char extension[10];
	char reportsTo[100];
	char jobTitle[50];
	char city[50];
};

int findEmployee(Connection* conn, int employeeNumber, struct Employee* emp);
void displayEmployee(Connection* conn, Employee emp);
void displayAllEmployees(Connection* conn);
void updateEmployee(Connection* conn, int employeeNumber);
void deleteEmployee(Connection* conn, int employeeNumber);
void insertEmployee(Connection* conn, Employee emp);

int main(void)
{
	// declare employee variable
	Employee employees = { '\0' };

	//OCCI variables
	Environment* env = nullptr;
	Connection* conn = nullptr;

	//User variables
	string str;
	string usr = "dbs211_202b21";
	string pass = "15057836";
	string srv = "myoracle12c.senecacollege.ca:1521/oracle12c";

	//create Environment and connection
	env = Environment::createEnvironment(Environment::DEFAULT);
	conn = env->createConnection(usr, pass, srv);

	// Main part of program
	int option;
	int done;
	int flag;
	int eNumber = 0;
	do
	{
		done = 1;
		option = menu(); // call menu function and get user's selection
		switch (option)
		{
		case 1:
			displayEmployee(conn, employees); // call displayEmployee function
			break;
		case 2:
			displayAllEmployees(conn); // call displayAllEmployees function
			break;
		case 3:
			insertEmployee(conn, employees);// call insertEmployee function
			break;
		case 4:
			updateEmployee(conn, eNumber);//call updateEmployee function
			break;
		case 5:

			deleteEmployee(conn, eNumber);// call deleteEmployee function
			break;
		case 6:
			std::cout << "Do you want to exit (y/n): ";
			flag = getSelection();
			if (flag)
			{
				done = 0;
				std::cout << "Exiting program!" << endl;
			}
			break;
		default:
			break;
		}
	} while (done);

	// terminate Environment and Connection
	env->terminateConnection(conn);
	Environment::terminateEnvironment(env);
	return 0;
}

//menu function to show title, menu and get user's selection
int menu(void)
{
	int selection = 0;

	// display title
	std::cout << "\n********************* HR Menu *********************" << endl;

	//show the menu for user to select
	std::cout << "1) Find Employee\n2) Employees Report\n3) Add Employee\n4) Update Employee\n5) Remove Employee\n6) Exit\n";
	std::cout << "Please choose option from 1 to 6: ";

	selection = getNumInRange(1, 6);//call getNumInRange function to validate user's selection
	return selection;
}

//validate user's input to make sure an integer is inputted
int getInt(void)
{
	int value;
	while (!(cin >> value))
	{
		std::cout << "Invalid Integer, try again: ";
		cin.clear();
		cin.ignore(2000, '\n');
	}
	if (cin) cin.ignore(2000, '\n');
	return value;
}

//validate user's input to make sure inputted integer is from 0 to 5
int getNumInRange(int min, int max)
{
	int done = 0, value;
	do
	{
		value = getInt();
		if (value < min || value > max)
		{
			done = 1;
			std::cout << "Invalid selection, try again: ";
		}
		else done = 0;
	} while (done);
	return value;
}

//get input from user for confirmation to exit 
int getSelection(void)
{
	int result = 0;
	int flag = 0;
	char check[4] = { '\0' };
	do {
		cin.get(check, 3);
		if ((check[0] != 'y' && check[0] != 'Y' && check[0] != 'N' && check[0] != 'n') || strlen(check) > 1)
		{
			check[0] = '\0';
			cin.clear();
			cin.ignore(2000, '\n');
			std::cout << "Invalid response, only (Y)es or (N)o are acceptable, retry: ";
			flag = 1;
		}
		else if (check[0] == 'Y' || check[0] == 'y')
		{
			result = 1;
			flag = 0;
		}
		else if (check[0] == 'N' || check[0] == 'n')
		{
			result = 0;
			flag = 0;
		}
	} while (flag);
	check[0] = '\0';

	return result;
}

//execute a query to find an employee
int findEmployee(Connection* conn, int employeeNumber, struct Employee* emp)
{
	int result = 0;
	Statement* stmt = conn->createStatement();
	stmt = conn->createStatement("SELECT e.employeeNumber,e.lastName,e.firstname,e.email,o.phone,e.extension,b.lastName || ' ' || b.firstName,e.jobtitle,o.city FROM dbs211_employees e LEFT JOIN dbs211_employees b ON e.reportsTo = b.employeeNumber LEFT JOIN dbs211_offices o ON e.officeCode = o.officeCode WHERE e.employeeNumber =:1");
	stmt->setInt(1, employeeNumber);
	ResultSet* rs = stmt->executeQuery();

	if (!rs->next()) result = 0;
	else
	{
		result = 1;
		rs = stmt->executeQuery();
		while (rs->next())
		{
			//store the data found to the variables
			int employeeNumber_ = rs->getInt(1);
			string lastName_ = rs->getString(2);
			string firstName_ = rs->getString(3);
			string email_ = rs->getString(4);
			string phone_ = rs->getString(5);
			string extension_ = rs->getString(6);
			string reports_to_ = rs->getString(7);
			string jobTitle_ = rs->getString(8);
			string city_ = rs->getString(9);

			//store employee data to Employee structure
			emp->employeeNumber = employeeNumber_;
			strcpy(emp->lastName, lastName_.c_str());
			strcpy(emp->firstName, firstName_.c_str());
			strcpy(emp->email, email_.c_str());
			strcpy(emp->phone, phone_.c_str());
			strcpy(emp->extension, extension_.c_str());
			strcpy(emp->reportsTo, reports_to_.c_str());
			strcpy(emp->jobTitle, jobTitle_.c_str());
			strcpy(emp->city, city_.c_str());
		}
	}

	//terminate statement
	conn->terminateStatement(stmt);
	return result;
}

// perform task once "Find Employee" option is selected
void displayEmployee(Connection* conn, Employee emp)
{
	std::cout << "Please enter a value for employee number: ";

	// call getInt() function to receive proper input from user
	int eNumber = getInt();

	// call findEmployee() to check whether the employee exists or not
	int result = findEmployee(conn, eNumber, &emp);

	if (result == 0)
	{
		std::cout << "Employee " << eNumber << " does not exist" << endl;
	}
	else
	{
		// display employee information
		std::cout << "---Employee found---" << endl;
		std::cout << "employeeNumber = " << emp.employeeNumber << endl;
		std::cout << "lastName = " << emp.lastName << endl;
		std::cout << "firstName = " << emp.firstName << endl;
		std::cout << "email = " << emp.email << endl;
		std::cout << "phone = " << emp.phone << endl;
		std::cout << "extension = " << emp.extension << endl;
		std::cout << "reportsTo = " << emp.reportsTo << endl;
		std::cout << "jobTitle = " << emp.jobTitle << endl;
		std::cout << "city = " << emp.city << endl;
	}
}

//perform task once "Employee Report" option is selected
void displayAllEmployees(Connection* conn)
{
	//Create the query by joining itselfa and joining with dbs211_office table
	Statement* stmt = conn->createStatement("SELECT e.employeenumber, e.firstname|| ' ' ||e.lastname,e.email,o.phone,e.extension, m.firstname|| ' '||m.lastname,e.jobtitle,o.CITY FROM DBS211_EMPLOYEES e LEFT OUTER JOIN DBS211_EMPLOYEES m ON e.reportsto = m.employeenumber LEFT OUTER JOIN DBS211_OFFICES o ON e.OFFICECODE = o.OFFICECODE ORDER BY e.employeenumber");
	ResultSet* rs = stmt->executeQuery();

	//display title for employee report
	std::cout << left << setfill(' ') << setw(6) << "E" << left << setw(20) << setfill(' ') << "Employee Name" << left << setw(35) << setfill(' ') << "Email" << left << setw(20) << setfill(' ') << "Phone" << left << setw(10) << setfill(' ') << "Ext" << left << setw(20) << setfill(' ') << "Manager" << endl;

	//Display the liner
	std::cout << left << setfill('-') << setw(105) << "-" << endl;

	while (rs->next())
	{
		//store the data of one row to the variable
		int employeenumber = rs->getInt(1);
		string employeeName = rs->getString(2);
		string email = rs->getString(3);
		string phone = rs->getString(4);
		string ext = rs->getString(5);
		string manager = rs->getString(6);

		//displayNum() and display() are two custom functions to display the content of a row
		displayNum(6, employeenumber);
		displayStr(20, employeeName);
		displayStr(35, email);
		displayStr(20, phone);
		displayStr(10, ext);
		displayStr(20, manager);
		std::cout << endl;
	}

	//terminate statement
	conn->terminateStatement(stmt);
}

//remove employee from provided employeeNumber
void deleteEmployee(Connection* conn, int employeeNumber)
{
	// declare and initialize Employee Structure to null
	Employee emp = { '\0' };

	std::cout << "Please enter a value for employee number to delete: ";
	employeeNumber = getInt();

	//call findEmployee function to check if entered employeenumber exist or not
	int check = findEmployee(conn, employeeNumber, &emp);

	if (!check) std::cout << "The employee does not exist." << endl;
	else
	{
		Statement* stmt = conn->createStatement();

		stmt->setSQL("DELETE FROM dbs211_employees WHERE employeenumber =:1");

		stmt->setInt(1, employeeNumber);

		stmt->executeUpdate();

		std::cout << "The employee is deleted." << endl;

		conn->terminateStatement(stmt);
	}
}

//updates the phone extension for the given employee
void updateEmployee(Connection* conn, int employeeNumber) {
	// declare and initialize Employee Structure to null
	Employee emp = { '\0' };

	// prompt user enter employee number which want to update extension
	std::cout << "Please enter a value for employee number to update: ";
	employeeNumber = getInt();

	//call findEmployee function to check if entered employeenumber exist or not
	int check = findEmployee(conn, employeeNumber, &emp);

	if (!check) cout << "The employee does not exist." << endl;

	else {
		Statement* stmt = conn->createStatement();

		stmt->setSQL("UPDATE dbs211_employees SET extension =:1 WHERE employeenumber =:2");

		string ext;

		cout << "Please enter the new phone extension: ";

		getline(cin, ext, '\n');

		stmt->setString(1, ext);

		stmt->setInt(2, employeeNumber);

		stmt->executeUpdate();

		std::cout << "The employee is updated." << endl;

		conn->terminateStatement(stmt);
	}
}
//insert a new employee
void insertEmployee(Connection* conn, Employee emp) {
	//check if the employee exists
	emp = { '\0' };
	int eNumber = 0;
	std::cout << "Employee Number: ";
	eNumber = getInt();
	int result = findEmployee(conn, eNumber, &emp);
	if (result == 1) {
		std::cout << "Employee with the same employee number exists" << std::endl;
	}
	else {
		emp.employeeNumber = eNumber;
		//get input from user and store it at Emp structure
		std::cout << "Last Name: ";
		cin.getline(emp.lastName, 50, '\n');

		std::cout << "First Name: ";
		cin.getline(emp.firstName, 50, '\n');

		std::cout << "Email: ";
		cin.getline(emp.email, 100, '\n');

		std::cout << "extension: ";
		cin.getline(emp.extension, 10, '\n');

		strcpy(emp.reportsTo, "1002");

		std::cout << "Job Title: ";
		cin.getline(emp.jobTitle, 50, '\n');

		std::cout << "City: ";
		cin.getline(emp.city, 50, '\n');

		//start the query
		Statement* stmt = conn->createStatement();
		stmt->setSQL("INSERT INTO dbs211_employees (employeeNumber, lastName, firstName, extension, email, officeCode, reportsTo, jobTitle) VALUES (:1,:2,:3,:4,:5,:6,:7,:8)");

		//convert values in char data type of emp struct to string data type

		string last_name(emp.lastName);
		string first_name(emp.firstName);
		string extension_(emp.extension);
		string email_(emp.email);
		string reports_to(emp.reportsTo);
		string job_title(emp.jobTitle);
		string city_(emp.city);

		//insert new Employee base on information from emp struct

		stmt->setInt(1, emp.employeeNumber);

		stmt->setString(2, last_name);

		stmt->setString(3, first_name);

		stmt->setString(4, extension_);

		stmt->setString(5, email_);

		stmt->setInt(6, 1);//office code = 1

		stmt->setString(7, reports_to);//reporstTo 1002,not 102, prof confirmed

		stmt->setString(8, job_title);

		stmt->executeUpdate();

		std::cout << "The new employee is added successfully." << std::endl;

		conn->terminateStatement(stmt);
	}

};
//format for displaying string
void displayStr(int width, string content)
{
	std::cout << left << setw(width) << setfill(' ') << content;
}

//format for displaying number
void displayNum(int width, const int content)
{
	std::cout << left << setw(width) << setfill(' ') << content;
}
